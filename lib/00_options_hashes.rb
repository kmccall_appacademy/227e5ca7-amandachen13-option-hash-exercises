# Options Hashes
#
# Write a method `transmogrify` that takes a `String`. This method should
# take optional parameters `:times`, `:upcase`, and `:reverse`. Hard-code
# reasonable defaults in a `defaults` hash defined in the `transmogrify`
# method. Use `Hash#merge` to combine the defaults with any optional
# parameters passed by the user. Do not modify the incoming options
# hash. For example:
#
# ```ruby
# transmogrify("Hello")                                    #=> "Hello"
# transmogrify("Hello", :times => 3)                       #=> "HelloHelloHello"
# transmogrify("Hello", :upcase => true)                   #=> "HELLO"
# transmogrify("Hello", :upcase => true, :reverse => true) #=> "OLLEH"
#
# options = {}
# transmogrify("hello", options)
# # options shouldn't change.
# ```

def transmogrify(str, options = {})
  defaults = {
    times: 1,
    upcase: false,
    reverse: false
  }

  option_hash = defaults.merge(options)
  transformed_str = str

  if option_hash[:upcase]
    transformed_str = transformed_str.upcase
  end

  if option_hash[:reverse]
    transformed_str = transformed_str.reverse
  end

  transformed_str * option_hash[:times]
end
